package com.bingewatch.service.user.microservicebingewatchuser.model;

public enum TypeRole {
    REGISTRERED, ADMINISTRATOR
}
